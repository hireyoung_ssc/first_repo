<?php include'include/head.php' ?>

<body data-spy="scroll" data-target="#main-navbar">
    <div class="page-loader"></div>  <!-- Display loading image while page loads -->
    <div class="body">

        <!--========== BEGIN HEADER ==========-->
        <?php include 'include/header.php'; ?>
        <!-- ========= END HEADER =========-->
    <section id="home" class=" bg-style1">
        <div id="mycarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                <img src="img/mainSlider.png" alt="" class="img-responsive" style="margin-top:350px;">
                   <div class="carousel-caption" >

                        <h1 style="color:black">We help young people start their careers</h1>
                        <div class="extra-space-l"></div>
                        <a class="btn btn-blank" href="signup_hy.php" role="button" >For Jobseeker</a>
                        <a class="btn btn-blank" href="signup_hy.php" role="button">For Companies</a>
                   </div>
                </div>
            </div>
        </div>
    </section>

      <!--  ********** Begining Block 2 ********** -->

<section id="jobseeker" class=" bg-style2">
    <div  id="" class="container">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                <!-- <hr class="section-heading-spacer"> -->
                <div class="clearfix"></div>
                <div class="extra-space-l"></div><div class="extra-space-l"></div>
                <h2 class="section-heading">Find and apply for internship </h2>
                <a class="btn btn-blank custom-btn" href="#" role="button">Join now</a>
            </div>
           <div class="col-lg-3 col-sm-pull-6  col-sm-6">
                <img class="img-responsive" src="img/icons/image1.png" style="width:250px;margin:25px;" alt="">
            </div>
        </div>

    </div>
</section>

<section id="find-course" class=" bg-style1">
<div  id="" class="container">
  <div class="row">
      <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
          <img class="img-responsive" src="img/icons/image3.png" alt="" style="width:250px;margin:25px;">
      </div>
      <div class="col-lg-5 col-sm-pull-6  col-sm-6">
        <div class="extra-space-l"></div><div class="extra-space-l"></div>
      <h2 class="section-heading"> Find events and courses  <br>to develop yourself</h2>
      <div class="extra-space-l"></div>
      <a class="btn btn-blank custom-btn" href="#" role="button">Join now</a>
      </div>
  </div>
</div>
</section>

<section id="get-help" class=" bg-style1">
<div  id="" class="container">
  <div class="row">
      <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
            <div class="extra-space-l"></div>  <div class="extra-space-l"></div>
            <h2 class="section-heading">Get personal help  <br>with tough issues</h2>
            <a class="btn btn-blank custom-btn" href="signup_hy.php" role="button">  Join now  </a>
      </div>
      <div class="col-lg-3 col-sm-pull-6  col-sm-6">
          <img class="img-responsive" src="img/icons/image4.png" alt="" style="width:250px; margin:25px;">
      </div>
  </div>
</div>
</section>
<section id="company" class=" bg-style1">
<div id="mycarousel" class="carousel slide" data-ride="carousel">
<div class="carousel-inner">
  <div class="item active">
  <img src="img/table.png" alt="" class="img-responsive" style="margin-top:280px;">
     <div class="carousel-caption" >

          <h2 style="color:black">Add your company and post your internships and work offers directly to your potential employees.</h2>
          <div class="extra-space-l"></div>
          <a class="btn btn-blank custom-btn" href="signup_hy.php" role="button">Join  now</a>
          <a class="btn btn-blank custom-btn2" href="signup_hy.php" role="button">Learn more</a>
     </div>
  </div>
</div>
</div>
</section>
    <section id="company">
        <div id="counter-up-trigger" class="counter-up text-white parallax" style="background:#50bdc6;">

                <!-- Begin page header-->
                <div class="page-header-wrapper">
                <div class="container">
                    <div class="page-header text-center wow fadeInDown" data-wow-delay="0.4s">
                        <div class="extra-space-l"></div>
                    </div>
                </div>
                </div>
                <!-- End page header-->
                <div class="container">

                        <div class="row">

                                <div class="fact text-center col-md-3 col-sm-6">
                                        <div class="fact-inner">
                                                <i class="fa fa-users fa-3x"></i>
                                                <div class="extra-space-l"></div>
                                                <span class="counter">1200</span>
                                                <p class="lead">Jobseekers </p>
                                        </div>
                                </div>

                                <div class="fact text-center col-md-3 col-sm-6">
                                        <div class="fact-inner">
                                                <i class="fa fa-briefcase fa-3x"></i>
            <div class="extra-space-l"></div>
                                                <span class="counter">21</span>
                                                <p class="lead">Companies </p>
                                        </div>
                                </div>

                                <div class="fact text-center col-md-3 col-sm-6">
                                        <div class="fact-inner">
                                                <i class="fa fa-file-text fa-3x"></i>
            <div class="extra-space-l"></div>
                                                <span class="counter">32</span>
                                                <p class="lead">Job Offers</p>
                                        </div>
                                </div>

                                <div class="fact last text-center col-md-3 col-sm-6">
                                        <div class="fact-inner">
                                                <i class="fa fa-book fa-3x"></i>
            <div class="extra-space-l"></div>
                                                <span class="counter">100</span>
                                                <p class="lead">Boosters</p>
                                        </div>
                                          <div class="extra-space-l"></div>
                                            <div class="extra-space-l"></div>
                                </div>

                        </div> <!-- /.row -->
                </div> <!-- /.container -->
        </div>

    </section>

			<section id="social-section">
        <div class="extra-space-l"></div>
          <div class="extra-space-l"></div>
        <!-- Begin page header-->
       <div class="page-header-wrapper">
           <div class="container">
               <div class="page-header text-center wow fadeInDown" data-wow-delay="0.4s">
                   <h2>Discover ultimate content on our pages</h2>

               </div>
           </div>
       </div>
       <!-- End page header-->


       <div class="container">
               <ul class="social-list">
                       <li> <a href="#" class="rotate-box-1 square-icon text-center wow zoomIn" data-wow-delay="0.3s"><span class="rotate-box-icon"><i class="fa fa-facebook"></i></span></a></li>
                       <li> <a href="#" class="rotate-box-1 square-icon text-center wow zoomIn" data-wow-delay="0.4s"><span class="rotate-box-icon"><i class="fa fa-vk"></i></span></a></li>
                       <li> <a href="#" class="rotate-box-1 square-icon text-center wow zoomIn" data-wow-delay="0.6s"><span class="rotate-box-icon"><i class="fa fa-linkedin"></i></span></a></li>
                       <li> <a href="#" class="rotate-box-1 square-icon text-center wow zoomIn" data-wow-delay="0.7s"><span class="rotate-box-icon"><i class="fa fa-facebook"></i></span></a></li>
           </ul>

       </div>
       <div class="extra-space-l"></div>
         <div class="extra-space-l"></div>
   </section>
<?php include'include/footer.php' ?>
